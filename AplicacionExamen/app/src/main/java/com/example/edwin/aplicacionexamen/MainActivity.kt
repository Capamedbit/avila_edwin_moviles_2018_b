package com.example.edwin.aplicacionexamen
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.widget.TextView;
import android.widget.Button;
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    internal lateinit var botonIniciar: Button;
    internal lateinit var botonParar: Button;
    internal lateinit var txtRandom: TextView;
    internal var tiempoRestante = 10;
    internal var puntaje = 0;
    internal var numeroRandom = -1;
    internal var juegoIniciado = false;
    internal lateinit var countDownTimer: CountDownTimer;
    internal val countDownInterval = 1000L;
    internal val initialCountDown = 10000L;
    internal lateinit var txtPuntajeJuego: TextView;



    internal val TAG = MainActivity::class.java.simpleName;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)//cada vez que se recarga la pantalla se instancia
        setContentView(R.layout.activity_main)//que e hagavisible el activiy main


        //estamos igualando el txt_viewp_puntahe en el txtPuntajeJuego
        txtPuntajeJuego = findViewById<TextView>(R.id.txt_view_puntaje);
        botonIniciar = findViewById<Button>(R.id.button_jugar);
        botonParar = findViewById<Button>(R.id.button_presionar);
        txtRandom = findViewById<TextView>(R.id.txt_view_numeroRandom);

        botonIniciar.setOnClickListener{ _ -> startGame()};
        botonParar.setOnClickListener{ _ -> incrementScore()};
        resetGame();
    }

    //validadcion en el botón
    private fun incrementScore(){
        if(numeroRandom == tiempoRestante){
            puntaje += 100;
        }else if (numeroRandom >= tiempoRestante-1 && numeroRandom <= tiempoRestante+1){
            puntaje += 50;
        }else{
            puntaje += 0;
        }
        txtPuntajeJuego.text = getString(R.string.puntaje, puntaje.toString());


        countDownTimer.cancel();//para que el contador se vuevla en cero
        endGame();
    }

    private fun iniciarContador(){
        countDownTimer = object : CountDownTimer(initialCountDown, countDownInterval){
            override fun onTick(millisUntilFinished: Long) {
                tiempoRestante = millisUntilFinished.toInt() / 1000;
            }

            override fun onFinish() {
                endGame();
            }
        }
    }

    private fun resetGame(){
        tiempoRestante = 10;
        numeroRandom =-1;
        txtRandom.text = "Número Random"
        juegoIniciado = false;

    }

    private fun startGame(){
        iniciarContador();
        if(numeroRandom==-1){//para uque no prima dos veces el valor de incio
            countDownTimer.start();
            numeroRandom = (0.. 10).shuffled().first();
            txtRandom.text= getString(R.string.numeroRandom, numeroRandom.toString());
            juegoIniciado = true;
        }else{
            Toast.makeText(this, getString(R.string.error_message), Toast.LENGTH_LONG).show();
        }
    }

    private fun endGame(){
        if(juegoIniciado) {
            Toast.makeText(this, getString(R.string.mensaje_juego_terminado, Integer.toString(puntaje)), Toast.LENGTH_LONG).show();countDownTimer.cancel();
            resetGame();
        }

    }

}
